<?php include("../includes/header-nav-first-use.php"); ?>
<div class="container-fluid">
	<div class="row">
		<!-- <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php //include("../includes/sidenav-my-settings.php"); ?>
            </div>
		</div> -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php //include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp first-use-activation">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Data cap limit reached</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top30">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Need more data?</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15 margin-bottom15">
                                        <p>It's easy to add more data to your Pay Monthly plan – just grab a Mobile Extra. Starting from only $6 for 60MB, you can buy them as a one-off 
                                        or on a monthly basis. And you can manage it all online.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Buy Extra">Buy Extra</button>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-xs-12 margin-top20">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Stay in control with data caps</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15">
                                        <p>All our Pay Monthly Plans come with data caps, so you stay in control of how much you spend.</p>
                                        
                                        <p>If you're getting close to your data limit, we’ll text you with a heads up. Then, if you do reach your 
                                        limit we'll automatically stop mobile data. The data cap works in NZ and if you’re overseas.</p>

                                        <p>It's free to change your data cap anytime – so you can keep using mobile data if you want to.</p>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-xs-12 margin-top15">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Change your data cap</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15 margin-bottom5">
                                        
                                        
                                        <p>You can increase or lower your NZ or roaming data cap anytime. </p>
                                        <p>Once you’ve made the change, restart your phone or put it into Flight Mode for 90 seconds and your new data cap will take effect immediately. </p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 margin-top10">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Top up online">change nz data cap</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom20">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Sign into mymobile">change roaming data cap</button>
                                    </div>
                                </div>
                                
                    		</div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer-first-use.php"); ?>