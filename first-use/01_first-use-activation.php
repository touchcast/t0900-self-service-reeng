<?php include("../includes/header-nav-first-use.php"); ?>
<div class="container-fluid">
	<div class="row">
		<!-- <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php //include("../includes/sidenav-my-settings.php"); ?>
            </div>
		</div> -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php //include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp first-use-activation">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Let's get started</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top30">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Setting up a new sim card?</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">     
                                    <div class="col-xs-12">
                                        <input id="" type="text" value="First name">
                                    </div>
                                </div>
                                <div class="row">     
                                    <div class="col-xs-12 margin-top20 margin-bottom20">
                                        <input id="" type="email" value="Email address">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Next">Next</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top30">
                                        <div class="no-padding">
                                            <h3 class="purple-title">Run out of credit?</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15 margin-bottom15">
                                        <p>It's simple to get going again. Grab a data Extra or top up.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Top up online">Get more data</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Sign into mymobile">Top Up</button>
                                    </div>
                                </div>
                                
                    		</div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer-first-use.php"); ?>