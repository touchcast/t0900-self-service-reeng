<?php include("../includes/header-nav-first-use.php"); ?>
<div class="container-fluid">
	<div class="row">
		<!-- <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php //include("../includes/sidenav-my-settings.php"); ?>
            </div>
		</div> -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php //include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp first-use-activation">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Let's get started</h3>
                                    </div>
                                </div>
                                <div class="row">     
                                                <div class="col-xs-12 text-center padding-top20">
                                                    <div class="myplans-info-box">
                                                        Thanks. We've received your details
                                                    </div>
                                                </div>
                                                
                                            </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top30">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Choose a prepaid value pack</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15">
                                        <p>Next, you'll need to pick a Prepaid Value Pack. They have all the good stuff like TXTs, data and minutes that you’ll need to get going. 
                                        Or, if you only plan on using your phone once in a while, go for our Casual Rate instead.</p>
                                        <p>Let's choose a pack.</p>
                                    </div>
                                </div>
                                       <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$9</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$9 Value pack</h3>
                                                                    <ul class="clearfix">
                                                                        <li>1,000 Texts + 30 Minutes</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="remain-bay-valuepack" id="product_valuepack" style="border-bottom: 0px;">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex month-value-bex-mymobile">
                                                                    <h2>$12</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$12 Text Data pack</h3>
                                                                    <ul class="clearfix">
                                                                        <li>50MB + 5,000 Texts</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                    </div>
                                                   
                                                    
                                                    
                                                  
                                                </div>
                                                
                                            </div><!--row-->
                                
                                <div class="row">
                                    <div class="col-xs-12 margin-top20">
                                        <div class="no-padding">
                                                <h3 class="purple-title">Or go casual?</h3>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 padding-top15 margin-bottom15">
                                        <p>With our casual option you only pay for what you use. You’ll just need to make sure your balance is topped up to cover it.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-bottom15 mymobile">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="col-xs-7 left-column casual-talk">TALK</td>
                                                    <td class="col-xs-3 col-offset-2">49c/min</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-7 left-column casual-talk">TEXT</td>
                                                    <td class="col-xs-3 col-offset-2">20c/msg</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-7 left-column casual-talk">DATA</td>
                                                    <td class="col-xs-3 col-offset-2">10MB $1/day</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-bottom20">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="Go Casual">Go Casual</button>
                                    </div>
                                </div>
                                
                                
                    		</div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer-first-use.php"); ?>