<div id='cssmenu' class="hidden-sm hidden-xs">
  <h3 class="side-nav-title">My Mobile</h3>
  <ul>
    <li class='active has-sub open'><a href='#'><span>My Usage</span></a>
      <ul style="display: block;">
        <li>
          <a href='#'><span>View Data Activity</span></a>
        </li>
      </ul>
    </li>
    <li class='has-sub'>
      <a href='#'><span>My Plan &amp; Extras</span></a>
      <ul>
         <li><a href="#" title="Buy Extras">Extras Purchased</a></li>
      </ul>
    </li>
    <li class='has-sub'>
      <a href='#'><span>Buy Extras</span></a>
      <ul>
         <li><a href="#" title="Buy Extras">Buy Extras</a></li>
      </ul>
    </li>
    <li class="has-sub">
      <a href='#'><span>Payments</span></a>
      <ul>
          <li><a href='#'><span>Top up a Prepaid</span></a></li>
      </ul>
    </li>
    <li class="has-sub">
      <a href='#'><span>My Settings</span></a>
      <ul>
        <li><a href='#'><span>Change Password</span></a></li>
      </ul>
    </li>
  </ul>
</div>