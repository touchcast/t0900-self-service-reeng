<footer>
	<!-- ///// Desktop Footer | same structure as  CQ ///// -->
	<div class="desktop-nav visible-lg visible-md hidden-sm hidden-xs">
		<div id="footer">
			<div class="footerchannel">
				<div id="footer_channel">
					<div id="mask"></div>
					<div class="help"><a href="javascript:window.print()"><i class="icon-doc-text-inv"></i>Print this page</a> </div>
					<a href="#top" class="top"><i class="icon-angle-circled-up"></i> Back to top</a> </div>
			</div>
			<div class="footerbreadcrumb">
				<div class="fullwidth-bg">
				</div>
			</div>

			<div class="bottom">
				<div class="wrap">
					<div class="row">
						<div class="col-xs-6">
							<ul>
								<li> <a href="http://www.sparknz.co.nz"> Spark New Zealand </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkdigital.co.nz"> Spark Digital </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkventures.co.nz"> Spark Ventures </a> </li>
								<li> &nbsp;|&nbsp; <a href="http://www.sparkfoundation.org.nz"> Spark Foundation </a> </li>
							</ul>
						</div>
						<div class="col-xs-6">
							<div class="copyright">© Copyright Spark 2014 All rights reserved</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!--Mobile Footer-->
    <div class="panel footer-mobile hidden-lg hidden-md visible-sm visible-xs">
	    <div class="footer-links"> <a class="t-c" href="http://www.spark.co.nz/terms">Terms and conditions</a> 
	      <!-- Copyright -->
	      <p>© 2015 Spark NZ</p>
	    </div>
	    <div class="bt_rs"> <a class="top-link" href="#scrollTop">TOP</a> </div>
	</div>

	<!-- Desktop footer END -->
</footer>
<!-- <script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>  -->
<script type="text/javascript" src="../js/data-activity.js"></script>
<script type="text/javascript" src="../js/sidebar.js"></script>
<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker({
        	minDate: moment(),
	      	format: 'DD/MM/YYYY'
	    });
    });
</script>
<script>
	$(function() {
	// see http://eonasdan.github.io/bootstrap-datetimepicker/
	var birthdatePickers = $('.birthdatePicker')
	if (birthdatePickers.length)
	birthdatePickers.datetimepicker(
	{
		pickTime: false,
		pickDate: false,
		maxDate: moment(),
		minDate: moment().subtract(100, 'y'),
		viewMode: 'years',
		useCurrent: false,
	}
	).on("dp.hide",function(e)
	{ $(this).parents('form.validate').bootstrapValidator('revalidateField', $(this)) }
	)
	})
</script>
</body>
</html>