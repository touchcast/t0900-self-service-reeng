<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Spark New Zealand - Configurator</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0" />
		<!-- <meta name="viewport" content="user-scalable=yes"/> -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<link rel="stylesheet" href="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive.css"/>
		<link rel="stylesheet" href="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive2.css"/>
		<link rel="stylesheet" href="http://www.spark.co.nz/content/dam/telecomcms/responsive/css/responsive-addon.css" type="text/css">
		
		<script type="text/javascript" src="http://www.spark.co.nz/etc/designs/spark-responsive/clientlib-responsive.js"></script>
		<script type="text/javascript" src="http://www.spark.co.nz/content/dam/telecomcms/responsive/js/responsive-addon.js"></script>


		<!-- Additional style for Mobile Re Eng configurator -->
		<!-- <link rel="stylesheet" href="../css/configurator/mobile-configurator.css" type="text/css"/> -->
		<!-- <link rel="stylesheet" type="text/css" href="../css/lib/datepicker.css"/> -->

		<!-- Additional font CSS for local renders -->
		<link rel="stylesheet" href="../css/font-addon.css" type="text/css"/>
		<!-- <link rel="stylesheet" href="../css/spark_publish.css" type="text/css"/> -->

		<link rel="stylesheet" href="../css/self-service.css" type="text/css"/>
		
		<!--Menu JS-->
		<script type="text/javascript" src="../js/menu.js"></script>
		<script type="text/javascript" src="http://www.spark.co.nz/etc/designs/tnz_selfservice/publish.js"></script>
		<script type="text/javascript" src="../js/slick.min.js"></script>
		<script type="text/javascript" src="../js/placeholder-shim.js"></script>
	</head>