<?php include("../includes/header-nav-payments.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <?php include("../includes/top-header-payments.php"); ?>
        
                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                </div>
                                <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="no-padding">
                                                            <h3 class="purple-title">Your bill</h3>
                                                    </div>
                                                    <p class="padding-top10">Account number: 123 456 789</p>
                                                </div>    
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <span class="bill-price">$0.00</span>
                                                    <p><a href="#">Change amount</a></p>
                                                </div>
                                                <div class="col-xs-6 text-right padding-top5">
                                                    <p>Due date: 27 Jul 2015</p>
                                                </div>    
                                            </div>
                                           <div class="row">     
                                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                                    <div class="myplans-success-box">
                                                        Good news!<br>You don't have to pay anything now. But you can always add credit to your account if you'd like.
                                                    </div>
                                                </div>
                                                
                                            </div>
                                             
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input name="productId" placeholder="" type="hidden">
                                                    <button class="bt-small-orange buy-extra-btn disabled" type="submit" title="PAY MY BILL">Pay now</button>
                                                </div>
                                            </div>  
                                               
                                            <br/>
                                            <div class="clear"></div>
                                            

                                            
                                             

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>