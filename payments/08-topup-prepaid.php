<?php include("../includes/header-nav-topup-prepaid.php"); ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <?php include("../includes/top-header.php"); ?>
        
                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                </div>
                                <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="no-padding">
                                                            <h3 class="purple-title">Please enter mobile number</h3>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">

                                                    <div class="row">
                                                    <div class="phone-number">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 margin-bottom20  padding-top15">
                                                            <input id="enter-mobile-number" type="text" placeholder="027 123 4568">
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    </div><!--row-->


                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="no-padding">
                                                            <h3 class="purple-title padding-top5">How would you like to top up?</h3>
                                                            <div class="opt_ol_opt" style="margin-top: 25px;">
                                                                <input class="opt_radio decoformbutton " type="radio" name="opt_topUp" placeholder="oneoff" id="opt_oneoff_topup">
                                                                <label for="opt_oneoff_topup" class="opt_radio_txt"><span></span>One-off debit/credit card top up</label>
                                                            </div>

                                                            <div class="opt_ol_opt">
                                                                <input class="opt_radio decoformbutton " type="radio" name="opt_topUp" placeholder="voucher" id="opt_voucher_topup">
                                                                <label for="opt_voucher_topup" class="opt_radio_txt"><span></span>Voucher top up</label> <br>
                                                            </div>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="row visible-lg visible-md hidden-sm hidden-xs">
                                                <div class="col-xs-12">
                                                    <div class="margin-bottom20">
                                                            <h3 class="purple-title">One-off top up</h3>
                                                            <p class="opt_radio_txt padding-top20">Please select an amount</p>
                                                            <div class="row margin-bottom20 padding-top20 select-amount">
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button">$10</a>  
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">    
                                                                    <a href="/onliness/extras--catalogue-/" class="top-up-amount active">$20</a>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button ">$40</a>  
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">    
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button">$60</a>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 last-btn amount">    
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button other-button">Other</a>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>    
                                            </div>

                                             <div class="row hidden-lg hidden-md visible-sm visible-xs">
                                                <div class="col-xs-12">
                                                    <div>
                                                            <h3 class="purple-title">One-off top up</h3>
                                                            <p class="opt_radio_txt padding-top20">Please select an amount</p>
                                                            <div class="row margin-bottom20 padding-top20 select-amount">
                                                                <div class="col-xs-3 amount">
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button">$10</a>  
                                                                </div>
                                                                <div class="col-xs-3 amount">    
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button top-up-amount active">$20</a>
                                                                </div>
                                                                <div class="col-xs-3 amount">
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button ">$40</a>  
                                                                </div>
                                                                <div class="col-xs-3 amount row-last-btn">    
                                                                    <a href="/onliness/extras--catalogue-/" class="add-button">$60</a>
                                                                </div>
                                                                <!-- <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 last-btn amount">    
                                                                    <a href="/onliness/extrascatalogue-/" class="add-button other-button">Other</a>
                                                                </div> -->
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 margin-bottom20 padding-left10">
                                                    <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 last-btn amount margin-top10">    
                                                        <a href="/onliness/extras--catalogue-/" class="add-button other-button">Other</a>
                                                    </div>
                                                </div>        
                                            </div>
                                            <!-- <div class="row hidden-lg hidden-md visible-sm visible-xs">
                                                
                                            </div> -->

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input name="productId" value="" type="hidden">
                                                    <button class="bt-small-orange buy-extra-btn" type="submit" title="PAY MY BILL">Continue</button>
                                                </div>
                                            </div>  
                                               
                                            <br/>
                                            <div class="clear"></div>
                                            

                                            
                                             

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>