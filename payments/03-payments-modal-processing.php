<?php include("../includes/header-nav-payments.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <?php include("../includes/top-header-payments.php"); ?>
        
                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                </div>
                                <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="no-padding">
                                                            <h3 class="purple-title">Let's grab your Spark bill</h3>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="row padding-top20">
                                                <div class="col-xs-12">

                                                    <div class="row">
                                                    <div class="phone-number">
                                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 margin-bottom20">
                                                            <input id="enter-spark-number" type="text" placeholder="123456789">
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    </div><!--row-->


                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input name="productId" placeholder="" type="hidden">
                                                    <button class="bt-small-orange buy-extra-btn" type="submit" title="PAY MY BILL">Continue</button>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-xs-12">
                                                   <div class="topup-prepaid">
                                                       <a class="prepaid-link" href="#">Top up a prepaid</a>
                                                   </div> 
                                                </div>

                                            </div>   
                                            <!-- Popup Modal -->
                                                <div class="row">
                                                    <div class="col-md-12 text-center padding-top20">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="add-button" data-toggle="modal" data-target="#myModal">
                                                          Launch Modal
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>   
                                     
                                            <div class="clear"></div>
                                            

                                            
                                             

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>





 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- <a type="button" class="close btn-mobile"><i class="icon-cancel-circle blue-text"></i></a> -->

    <div class="modal-dialog adding-number-modal">
        <div class="modal-content">
             <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



          <!-- <div class="modal-header">
            
          </div> -->

          <div class="modal-body text-center">
             <!-- <a href="#" class="float-right clearfix" data-dismiss="modal">
                Close <i class="icon-x"></i>
            </a> -->

            <!-- <span class="adding-number"><b>Adding a number</b></span>

            <p class="adding-number">Happy to add 027 123 4568 <br/> to My Favourites?</p> -->

            <div class="row">
                <div class="col-xs-12">
                    <span style="display:block;" class="margin-bottom20">
                        <img id="spinner-image" src="../images/spark_processing.gif">
                        <br><span class="spinner-txt">Processing...</span> 
                    </span>
                </div>

            </div>
              
            <!-- <a href="#" class="button blue"> call to action</a> -->
            <!-- <div class="modal-header">
                
            </div> -->
          </div>
          
        </div>
    </div>
</div>
<!-- /End Popup Modal -->






                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>