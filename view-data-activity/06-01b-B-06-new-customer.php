<?php include("../includes/header-nav-my-usage.php"); ?>

<div class="container-fluid">
	<div class="row data-activity">
		<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php include("../includes/sidenav-view-data-activity.php"); ?>
            </div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
            <?php include("../includes/top-header-view-data-activity.php"); ?>
            <div class="par parsys">
            	<div class="myusage section">
            		<div class="data-module-comp">
                        <div class="row panel bar-module data-module">
                            <div class="col-xs-12">
                                <h3 class="bar-module-headline-data">Usage for the last few days</h3>
                            </div>
                        </div>
                        <a class="panel-link" href="#">
                            <div class="remain padding-top10">
                                
                            </div>
                        </a>

                        <!-- Carousel Content Starts -->
                          <div id="myResponsiveTabContent" class="tab-content daily-usage">
                            <div class="tab-pane fade in active" id="categories">
                                <div class="card secondary custom-carousel no-margin">
                                        <div class="carousel carousel-list carousel-gallery">
                                          <div>
                                                <a href="#">
                                                    <div class="carousel-items">
                                                        <div class="chart">
                                                            <ul id="bars">
                                                                <li>
                                                                  <div data-percentage="35" class="bar" data-name="5.11GB">
                                                                    <p class="data-size light-orange">
                                                                        <span class="data-num">140</span>
                                                                        <span class="data-text">MB</span>
                                                                    </p>
                                                                </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </a>
                                            <p class="monthly-usage">
                                              <span class="month extra-bold no-margin">24 June</span>
                                              <span>Wed</span>
                                            </p>
                                        </div>

                                        <div>
                                            <a href="#">
                                                <div class="carousel-items">
                                                    <div class="chart">
                                                        <ul id="bars">
                                                            <li>
                                                                <div data-percentage="50" class="bar" data-name="140&#0013;MB">
                                                                    <p class="data-size light-orange">
                                                                        <span class="data-num">280</span>
                                                                        <span class="data-text">MB</span>
                                                                    </p>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                            <p class="monthly-usage">
                                              <span class="month extra-bold no-margin">25 June</span>
                                              <span>Thu</span>
                                            </p>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <div class="carousel-items">
                                                   <div class="chart">
                                                        <ul id="bars">
                                                            <li>
                                                              <div data-percentage="50" class="bar" data-name="300MB">
                                                                <p class="data-size light-orange">
                                                                    <span class="data-num">297</span>
                                                                    <span class="data-text">MB</span>
                                                                </p>
                                                              </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="arrow-down"></div>
                                            <p class="monthly-usage">
                                              <span class="month extra-bold no-margin">26 June</span>
                                              <span>Today</span>
                                            </p>
                                        </div>                                        
                                    </div>
                                    <div class="empty-grey-bar"></div>
                                    <div class="inner-card">
                                        <div class="link-group ticker padding-left-5">
                                              <ul>
                                                <h3 class="purple-text no-margin">Today <span>as of 9.35AM</span></h3>
                                                <div class="col-xs-12 col-sm-6 no-padding right-box-padding-7">
                                                    <h5>Included in my plan &amp; Extras</h5>

                                                    <div class="col-xs-12 light-grey-bg">
                                                      <img class="col-sm-1 col-xs-1 no-padding" src="../images/orange-nz.png"/>
                                                      <span class="col-sm-7 col-xs-7 padding-left-5 custom-col">NZ</span>
                                                      <span class="col-sm-3 col-xs-3 extra-bold right-align padding-right-0">297MB</span>
                                                    </div>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <!-- Carousel Content Ends -->
          		        </div>

                    <!-- Stacked Tab Carousel -->
                    <div class="data-module-comp no-margin">
                        <div class="row panel bar-module data-module">
                            <div class="col-xs-12">
                                <h3 class="bar-module-headline-data">Usage for the Last few Months</h3>
                            </div>
                        </div>
                        <a class="panel-link" href="#">
                            <div class="remain padding-top10">
                                
                            </div>
                        </a>

                        <!-- Carousel Content Starts -->
                         <div id="myResponsiveTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="categories">
                              <div class="card secondary custom-carousel">
                                  <div class="carousel carousel-list carousel-gallery">
                                    <div>
                                          <a href="#">
                                              <div class="carousel-items">
                                                  <div class="chart">
                                                     <ul id="bars">
                                                          <li class="graph-container">
                                                              <div class="bar-graph" data-name="140&#0013;MB">
                                                                  <div class="colWrapper">
                                                                    <div class="barContainer">
                                                                       <div data-percentage="0" class="empty stacked"></div>
                                                                        <div data-percentage="0" class="empty stacked"></div>
                                                                        <div data-percentage="0" class="empty stacked"></div>
                                                                        <div data-percentage="95" class="empty stacked"></div>
                                                                        <div data-percentage="5" class="stacked light-orange"></div>
                                                                    </div>
                                                                </div>
                                                              </div>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </a>
                                          <div class="arrow-down"></div>
                                          <p class="monthly-usage">
                                              <span>717MB</span>
                                              <span class="month extra-bold">June</span>
                                              <span class="small-text">16 Jun - 15 Jul</span>
                                            </p>
                                      </div>
                                  </div>

                                   <div class="empty-grey-bar-stacked"></div>
                                  <div class="inner-card">
                                      <div class="link-group ticker padding-left-5">
                                            <ul>
                                                <h3 class="purple-text no-margin">This month <span>so far</span></h3>
                                                <div class="col-xs-12 col-sm-6 no-padding right-box-padding-7">
                                                    </h3><p>&nbsp;</p>
                                                   
                                                    <div class="col-xs-12 light-grey-bg">
                                                      <img class="col-sm-1 col-xs-1 no-padding" src="../images/orange-nz.png"/>
                                                      <span class="col-sm-7 col-xs-7 padding-left-5 custom-col">NZ</span>
                                                      <span class="col-sm-3 col-xs-3 extra-bold right-align padding-right-0">717MB</span>
                                                    </div>
                                                    
                                                </div>
                                                
                                            </ul>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <!-- Carousel Content Ends -->
                    </div>
                    
            	</div>
            </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include("../includes/footer.php"); ?>