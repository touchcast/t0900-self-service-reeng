<?php include("includes/header-myusage.php"); ?>

<!--googleon: index-->
<div class=" off-canvas  hide-extras ">

<script type="text/javascript" src="http://www.spark.co.nz/etc/clientlibs/granite/jquery/noconflict.js"></script>
<noscript>
        </noscript>
    <div id="imgLoading" class="loading" style="display:none;">
	<img src="http://www.spark.co.nz/etc/designs/tnz_selfservice/publish/img/fancybox/loading.gif" alt="Loading..."/>
 	</div>
 	<div class="fancybox-overlay fancybox-overlay-fixed" style="display: none;"></div>
    <!-- 
<div id="content" class="tnz clearfix">
 -->
 <div id="page" role="main" class="">
	<!-- Mobile Header -->
	 <div id="mobile-header-container" class="row">
		<a name="scrollTop"></a>
		<div class="smartphone-nav"> 
			<a class='sidebar-button' id="sidebarButton" href="#offcanvas"></a>
			<h1>My Usage</h1>
			
			<a class="reload-button" href="?"></a>
		</div>
		
		<div class="non-css3-nav">
			<a class="sidebar-button" href="#mobile-non-js-nav"></a>
			<h1>My Usage</h1>
			<a class="reload-button" href="?"></a>
		</div>
	</div>	
	<!-- End Mobile Header -->

	<div id="content-container" class="row">

		<div class="leftnavigation navigation"><!-- Navigation -->
	<div id="leftmarginal">
		<h2 class="side-nav-title">My Mobile</h2>
		<ul class="side-nav">
				<li >
						<a href="/onliness/myspark/" title="MySpark">MySpark</a></li>
				<li class="active">
						<a href="/onliness/my-usage/" title="My Usage">My Usage</a></li>
				<li >
						<a href="/onliness/plan---extras/" title="My Plan & Extras">My Plan & Extras</a></li>
				<li >
						<a href="/onliness/myaccount" title="My Account">My Account</a></li>
				<li >
						<a href="/onliness/top-up/" title="Top Up">Top Up</a></li>
				<li >
						<a href="/onliness/extras--catalogue-/" title="Buy Extras">Buy Extras</a></li>
				<li >
						<a href="/onliness/view-activity/" title="View Activity">View Activity</a></li>
				<li >
						<a href="/onliness/recent-transactions/" title="Recent Transactions">Recent Transactions</a></li>
				<li >
						<a href="/onliness/help-and-support/" title="Help & Support">Help & Support</a></li>
				<li >
						<a href="/onliness/signout/" title="Sign Out">Sign Out</a></li>
				</ul>
		</div>	

		
	<!-- End Self-Service Navigation --> 

</div>
<div id="maincontent">
			<div class="mybalance"><!-- My Usage Panel -->
			<div class="panel mu_balance">
				<input type="hidden" id="myBalanceNumberParam" value="number"/>
				<input type="hidden" id="myBalanceGoToParam" value="url"/>
				<input type="hidden" id="currentPageURI" value="/onliness/my-usage.html"/>
				<div class="cb_mu_ls your_balance">
		        	<h4>Your balance :</h4>
		        	<p class="your-mobile-number">027 485 6970</p>
		    	</div>
	    		<div class="cb_mu_m" id="balance"><h5>$0.00</h5></div>
		    	<div id="topup_button" class="topup_button">
		    	<div class="bt_rs topup">
								<form action="/onliness/top-up.html" method="GET">
									<input name="productId" value="" type="hidden"/>
									<button class="bt-small-orange" type="submit" title="TOP UP">TOP UP</button>
								</form>
							</div>
						</div>
		    	<div class="cb_mu_m" id="balance_after"><h5>$0.00</h5></div>   
               
			</div>		
			<div class="panel your-mobile-content">
				<p class="date">As at 4:37pm Tuesday 14 Jul 2015</p>
				<p class="your-mobile-number">027 485 6970</p>
			</div>
			<!-- End My Usage Panel -->
		</div>
<div class="error-msg" id="internalError" style="display:none">
                <p id="internalErrorText"></p>
            </div>
			<div class="par parsys"><div class="myusage section"><div id="messagesDiv">
    </div> 
    <!-- start : #messages-lightbox -->
	<a href="#messages-lightbox" id="messages-lightbox-hook">&nbsp;</a>
	<div style="display: none">
	<div id="messages-lightbox">&nbsp;</div>
	</div>
	<!-- // end : #messages-lightbox -->
	<div class="data-module-comp">
					<!-- Usage Panel -->
					<div class="panel bar-module data-module">
						<div class="cb_mu_ls">
	<h3 class="bar-module-headline-data">Data</h3>
		</div>
<div class="bt_rs">
							<a href="/onliness/extras--catalogue-/" class="add-button">ADD</a>
						</div>
					</div>
					
					<a href="/onliness/plan---extras/#1769943107">
				<div class="remain">
					<h3>$0 Socialiser</h3>
						<h4 class="renew-date">Renews 25 Jul 2015 - 11 Days Left</h4>
						<span class="remaining-data">275MB used</span>
		
					<span class="expires-data">749MB left</span>
								<div class="progress">
								<span class="meter" style="width: 27%"></span>
								</div>
							
							<div>
								<!-- No overusage -->
										<span class="usage-label"
											style="text-align: right; float: right">1024</span>
									</div>
		
						<hr>
					</div>
			</a>
		<a href="/onliness/plan---extras/#1666123634">
				<div class="remain">
					<h3>750MB - $29 Value Pack</h3>
						<h4 class="renew-date">Renews 14 Aug 2015 - 31 Days Left</h4>
						<span class="remaining-data">2MB used</span>
		
					<span class="expires-data">748MB left</span>
								<div class="progress">
								<span class="meter" style="width: 1%"></span>
								</div>
							
							<div>
								<!-- No overusage -->
										<span class="usage-label"
											style="text-align: right; float: right">750</span>
									</div>
		
						</div>
			</a>
		<!-- End Usage Panel -->					
				</div>
			<div class="data-module-comp">
					<!-- Usage Panel -->
					<div class="panel bar-module data-module">
						<div class="cb_mu_ls">
	<h3 class="bar-module-headline-txt">Text</h3>
		</div>
<div class="bt_rs">
							<a href="/onliness/extras--catalogue-/" class="add-button">ADD</a>
						</div>
					</div>
					
					<a href="/onliness/plan---extras/#1666123634">
				<div class="remain">
					<h3>Unlimited Texts - $29 Value Pack</h3>
						<h4 class="renew-date">Renews 14 Aug 2015 - 31 Days Left</h4>
						<span class="remaining-data">0 TEXT used, UNLIMITED left</span>
		
					</div>
			</a>
		<!-- End Usage Panel -->					
				</div>
			<div class="data-module-comp">
					<!-- Usage Panel -->
					<div class="panel bar-module data-module">
						<div class="cb_mu_ls">
	<h3 class="bar-module-headline-calls">Voice</h3>
		</div>
<div class="bt_rs">
							<a href="/onliness/extras--catalogue-/" class="add-button">ADD</a>
						</div>
					</div>
					
					<a href="/onliness/plan---extras/#1666123634">
				<div class="remain">
					<h3>150 Minutes - $29 Value Pack</h3>
						<h4 class="renew-date">Renews 14 Aug 2015 - 31 Days Left</h4>
						<span class="remaining-data">0 Min used</span>
		
					<span class="expires-data">150 Mins left</span>
								<div class="progress">
								<span class="meter" style="width: 0%"></span>
								</div>
							
							<div>
								<!-- No overusage -->
										<span class="usage-label"
											style="text-align: right; float: right">150</span>
									</div>
		
						</div>
			</a>
		<!-- End Usage Panel -->					
				</div>
			</div>

</div>
</div>
	 	
	 	<div id="rightmaginal">
	 	
		 	<!-- right content -->
		 	<div class="yourmobile"><div class="panel yph-right">
		<h3>Your mobile</h3>
		<input type="hidden" id="yourMobileNumberParam" value="number"/>
		<input type="hidden" id="yourMobileGoToParam" value="url"/>
		<form method="get" action="/rest/v1/access/landingpage">
		<input type="hidden" id="pageURI" name="url" value="/onliness/my-usage.html"/>
		<p>027 485 6970</p>
		</form>   	
	</div>	
</div>
</div>
	</div>
 	
	<!--  start : mobile footer -->
	<div class="panel footer-mobile">
	    <div class="footer-links"> <a class="t-c" href="http://www.spark.co.nz/terms">Terms and conditions</a> 
	      <!-- Copyright -->
	      <p>© 2013 Spark NZ</p>
	    </div>
	    <div class="bt_rs"> <a class="top-link" href="#scrollTop">TOP</a> </div>
	</div>
	<!--  // end : mobile footer -->	
 	
	
	
</div>

<div class="show-for-medium-down" role="complementary" id="sidebar"> 
		<!-- Start OffCanvas Navigation -->
		<div class="main-menu show-for-medium-down">
		    <nav role="navigation" id="sideMenu">
				<div class="username">
			            <h3></h3>
			            <p>027 485 6970</p>
			        </div>
		        
					<!-- Self Service Navigation - Smartphone with CSS3 -->
					
					<ul class="nav-bar" id="sideMainNav">
					  	<li ><a href="/onliness/myspark/">MySpark</a></li>
						<li ><a href="/onliness/my-usage/">My Usage</a></li>
						<li ><a href="/onliness/plan---extras/">My Plan & Extras</a></li>
						<li ><a href="/onliness/myaccount">My Account</a></li>
						<li ><a href="/onliness/top-up/">Top Up</a></li>
						<li ><a href="/onliness/extras--catalogue-/">Buy Extras</a></li>
						<li ><a href="/onliness/view-activity/">View Activity</a></li>
						<li ><a href="/onliness/recent-transactions/">Recent Transactions</a></li>
						<li ><a href="/onliness/help-and-support/">Help & Support</a></li>
						<li ><a href="/onliness/signout/">Sign Out</a></li>
						</ul>
				<!-- End Mobile Self-Service Navigation -->
			</nav>
	    </div>
	</div>
</div>

<!--googleoff: index-->

<?php include("includes/footer-myusage.php"); ?>
