(function() {
	$(document).ready(function(){
		
		var sidebar = $('.sidebar-floater');
		var content = $('.content-container');
		var originalContentHeight = content.height();
		var summary = $('.summary-header-block-2');
		var summaryTop = summary.offset().top;
		var floater = $('.sidebar-floater');
		var summaryHeight = 20+summary.height()+5+16;
		var totalContainer = $('.total-container');

		//extend page height if sidebar is longer
		$('#accordion').on('shown.bs.collapse hidden.bs.collapse', function () {
			if((sidebar.height() + summaryHeight) > content.height()) {
				content.height(sidebar.height() + summaryHeight);
			}
			else {
				content.height(originalContentHeight);
			}
		});

		//sidebar scrolling magic
		$(window).scroll(function () {
			var scroll = $(this).scrollTop();

			if(scroll >= summaryTop) {
				summary.css({
					'position':'fixed',
					'top':'0',
					'width':'301px'
				});
				floater.css({
					'position':'absolute',
					'top': summaryHeight + 'px',
					'width':'301px'
				});
			}
			else {
				summary.css({
					'position':'relative'
				});
			}

			if(scroll > sidebar.height() - totalContainer.height() + 75) {
				floater.css({
					'position':'fixed',
					'top':summaryHeight - $('#accordion').height() + 'px'
				});
			}
			else {
				floater.css({
					'position':'absolute',
					'top': summaryHeight + 'px',
					'width':'301px'
				});
			}
		});
	});
}());