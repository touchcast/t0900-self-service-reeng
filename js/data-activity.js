// ------------------------------------------------------------------------------
// Data activity Slick
// ------------------------------------------------------------------------------
$(document).ready(function(){
  $('.carousel-gallery').slick({
    slidesToShow: 8,
    infinite: false,
    slidesToScroll: 1,
    // initialSlide:1,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: false,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },

        {
          breakpoint: 320,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }
    ]
  });
  function goToSlide(){
    if ($(window).width() < 524) {
      slideIndex = $('.slick-slide:last-child').index();
      $( '.carousel-gallery' ).slick('slickGoTo','8');
    }

    if ($(window).width() < 668) {
      slideIndex = $('.slick-slide:last-child').index();
      $( '.carousel-gallery' ).slick('slickGoTo','4');
    }

    else{
      slideIndex = $('.slick-slide:last-child').index();
      $( '.carousel-gallery' ).slick('slickGoTo','1');
    }
  }goToSlide();


  var resizeID;
  var width = $(window).width();
  $(window).resize(function(){
    if($(this).width() != width){

      if ($(window).width() < 668) {
        goToSlide();
      slideIndex = $('.slick-slide:last-child').index();
      $( '.carousel-gallery' ).slick('slickGoTo','8');
    }
      clearTimeout(resizeID);
      resizeID = setTimeout(goToSlide, 500);
    }
  });
});

// ------------------------------------------------------------------------------
// Data activity charts
// ------------------------------------------------------------------------------
$(function() {
  $("#bars li .bar").each( function( key, bar ) {
    var percentage = $(this).data('percentage');
    
    $(this).animate({
      'height' : percentage + '%'
    }, 1000);
  });
});

$(function() {
    setTimeout(function(){ 
        $(".barContainer").each( function( key, bar ) {
            $(this).animate({
              'bottom' : '0px'
            }, 1000);
        });
    }, 1000);
});

$(function() {
    setTimeout(function(){ 
        $(".stacked").each( function( key, bar ) {
            var percentage = $(this).data('percentage');
            $(this).css('height', percentage + '%');
            $(this).animate({
              'bottom' : '0px'
            }, 1000);
        });
    }, 1000);
});
