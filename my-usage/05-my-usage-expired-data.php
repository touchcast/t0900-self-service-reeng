<?php include("../includes/header-nav-my-usage.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php include("../includes/sidenav-my-usage.php"); ?>
            </div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                            <div class="row">     
                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                    <div class="error">
                                        Looks like your account has expired or been switched off. Sorry about that.<br>But call us on <u>123</u> and we can sort it out.
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer.php"); ?>