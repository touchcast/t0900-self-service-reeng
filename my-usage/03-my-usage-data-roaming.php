<?php include("../includes/header-nav-my-usage.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php include("../includes/sidenav-my-usage.php"); ?>
            </div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp">
                    			<div class="row panel bar-module data-module">
                    				<div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Data Roaming</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs fit">
                                            <a href="#" class="add-button">Change limit</a>
                                        </div>
                                    </div>
                    			</div>
                            	<a class="panel-link" href="#">
                            		<div class="remain padding-top10">
                                		<h3>Your roaming cost so far</h3>
										<span class="remaining-data">$80 used</span>
										<span class="expires-data">$70 left</span>
										<div class="progress">
											<span class="meter" style="width: 60%"></span>
										</div>
										<div class="pull-right">
											$150 total
										</div>
                                	</div>
                            	</a>
                                <div class="row">     
                                    <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                        <div class="myplans-info-box">
                                            Roaming overseas? It's simple to control your data costs.<br>Your current limit is $150. Change your limit in <a href="#">My Settings</a>.
                                        </div>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Data</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Add</a>
                                        </div>
                                    </div>
                                </div>
                                <a class="panel-link" href="#">
                                    <div class="remain padding-top10">
                                        <h3>5GB - $79 Ultra Plus Mobile</h3>
                                        <span class="remaining-data">2560MB used</span>
                                        <span class="expires-data">2560MB left</span>
                                        <div class="progress">
                                            <span class="meter" style="width: 50%"></span>
                                        </div>
                                        <div class="pull-right">
                                            5120MB total
                                        </div>
                                    </div>
                                </a>
                                <div class="row panel bar-module data-module">
                    				<div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Text</h3>
                                    </div>
                    			</div>
                                <div class="row">     
                                    <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                        <div class="myplans-info-box">
                                            No limits here. Send as many as you like!
                                        </div>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                    				<div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Voice</h3>
                                    </div>
                    			</div>
                                <div class="row">     
                                    <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                        <div class="myplans-info-box">
                                            No limits here. Make all the calls you want!
                                        </div>
                                    </div>
                                </div>
                    		</div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer.php"); ?>