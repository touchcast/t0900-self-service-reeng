<?php include("../includes/header-nav-my-plans.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <!--Top Header-->
                                <?php include("../includes/top-header.php"); ?>

                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                    </div>
                                    <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">My Plan</h3>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="bt_rs">
                                                    <a href="/onliness/extras--catalogue-/" class="add-button">CHANGE</a>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                                    <div class="error">
                                                        Oops! Looks like we can’t retrieve your plan details right now. Sorry about that. Don’t worry: you still have a plan, it’s just not displaying. Try signing in again or please come back later.

                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="clear"></div>
                                        <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">Extras</h3>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="bt_rs">
                                                    <a href="/onliness/extras--catalogue-/" class="add-button">ADD</a>
                                                </div>
                                                </div>
                                            </div>
                                            
                                        <div class="remain padding-top10">
                                            <h3>$15 Data</h3>
                                                <h4 class="renew-date padding-top8">Renews on 17 Jul 2015 - 9 days left</h4>
                                                <div class="row">
                                                    <div class="col-xs-3 no-padding">
                                                        <span class="remaining-data">
                                                            Data
                                                        </span>

                                                    </div>    
                                                    <div class="col-xs-4">
                                                        <span class="expires-data">
                                                            700MB
                                                        </span>
                                                    </div>
                                                </div>
                                                <button class="planextras-bttn nongr" type="submit" id="submit-1666123634" title="DON'T RENEW">DON'T RENEW</button>

                                                <hr>
                                                <h3>$6 My Favourites</h3>
                                                <h4 class="renew-date padding-top8">Renews on 17 Jul 2015 - 9 days left</h4>
                                                <div class="row no-paddin">
                                                    <div class="col-xs-3 no-padding">
                                                        <span class="remaining-data">
                                                            Number<br>
                                                            Free mins
                                                        </span>

                                                    </div>    
                                                    <div class="col-xs-5 my-favourites">
                                                        <span class="expires-data">
                                                            027 123 4569<br>
                                                            Up to 2 hours
                                                        </span>
                                                    </div>
                                                </div>
                                                <button class="planextras-bttn nongr" type="submit" id="submit-1666123634" title="DON'T RENEW">DON'T RENEW</button>

                                                <hr>
                                            </div>  

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>