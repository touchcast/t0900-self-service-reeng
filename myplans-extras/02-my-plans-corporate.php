<?php include("../includes/header-nav-my-plans.php"); ?>

        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                            <!--Top Header-->
                            <?php include("../includes/top-header.php"); ?>
             
                                    <div class="par parsys"><div class="myusage section">
                            
                            <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">My Plan</h3>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="bt_rs">
                                                    <a href="/onliness/extras--catalogue-/" class="add-button">CHANGE</a>
                                                </div>
                                                </div>
                                            </div>
                                             <div class="row plans padding-top20">
                                             <h3 class="purple-title" style="border: none;">Corporate plan name</h3>
                                                <table>
                                                    <tr>
                                                        <td class="col-xs-3 left-column">Talk</td>
                                                        <td class="col-xs-9">Unlimited mins to NZ & Australia numbers</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-xs-3 left-column">Text</td>
                                                        <td class="col-xs-9">Unlimited</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-xs-3 left-column">Data</td>
                                                        <td class="col-xs-9">5GB</td>
                                                    </tr>
                                                </table>
                                            </div><!--row-->  
                                            <!-- <div class="row">
                                                <div class="col-xs-11">
                                                   
                                                        <div class="remain padding-top10">
                                                            <h3>Corporate plan name</h3>
                                                                <div class="row">
                                                                    <div class="col-xs-3">
                                                                        <span class="remaining-data">
                                                                            Talk<br>
                                                                            Text<br>
                                                                            Data

                                                                        </span>
                                                                    </div>    
                                                                    <div class="col-xs-9">
                                                                        <span class="expires-data">
                                                                            Unlimited mins to NZ & Australia numbers<br>
                                                                            Unlimited<br>
                                                                            5GB
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                   
                                                </div>
                                            </div> --><!--row-->
                                             <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="panel additional-cost">
                                                        <a class="additional-link" data-id="additional-cost-hook" href="#" style="font-weight: normal;">
                                                        Additional charges and details</a>
                                                    </div>
                                                    <div class="additional-remain" id="additional-cost" style="display: block;">
                                                        <ul>
                                                            <li>Additional data - $0.30/MB</li>
                                                            <li>Voicemail cost - $0.20</li>
                                                            <li>Additional Minutes - $0.69/Min</li>
                                                            <li>Calling International Top 5 - $0.91/Min</li>
                                                            <li>Calling Rest of World - $1.43/Min</li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                       <br><br>                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>