<?php include("../includes/header-nav-my-plans.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                            <!--Top Header-->
                            <?php include("../includes/top-header.php"); ?>
                      
                                <div class="par parsys"><div class="myusage section">


                            
                            <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">My Plan</h3>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="bt_rs">
                                                        <a href="/onliness/extras--catalogue-/" class="add-button">CHANGE</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                   
                                                        <div class="remain padding-top10">
                                                            <h3>$79 /mth - Open term</h3>
                                                                <div class="row plans-my-extras">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="col-xs-3 left-column">Talk</td>
                                                                            <td class="col-xs-9">Unlimited mins to NZ & Australia numbers</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="col-xs-3 left-column">Text</td>
                                                                            <td class="col-xs-9">Unlimited</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="col-xs-3 left-column">Data</td>
                                                                            <td class="col-xs-9">5GB</td>
                                                                        </tr>
                                                                    </table>
                                                                </div><!--row-->        
                                                                <!-- <div class="row">

                                                                    <div class="col-xs-3">
                                                                        <span class="remaining-data">
                                                                            <p>Talk</p>
                                                                            <p>Text</p>
                                                                            <p>Data</p>

                                                                        </span>
                                                                    </div>    
                                                                    <div class="col-xs-9">
                                                                        <span class="expires-data">
                                                                            <span>Unlimited mins to NZ & Australia numbers</span>
                                                                            <span>Unlimited</span>
                                                                            <span>5GB</span>
                                                                        </span>
                                                                    </div>
                                                                </div> --><!--row-->
                                                                <div class="row">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="openterm"><i class="icon-wifi"></i></td>
                                                                            <td style="padding-top: 5px;" class="openterm-text">1GB WiFi /day </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="openterm"><i class="icon-thanks"></i></td>
                                                                            <td class="openterm-text">Thanks</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="openterm"><i class="icon-music"></i></td>
                                                                            <td class="openterm-text">Spotify Premium</td>
                                                                        </tr>
                                                                    </table>
                                                                </div><!---->
                                                            </div><!--remain-->
                                                   
                                                </div>
                                                <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                </div> -->
                                            </div><!--row-->
                                             <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="panel additional-cost">
                                                        <a class="additional-link" data-id="additional-cost-hook" href="#" style="font-weight: normal;">
                                                        Additional charges and details</a>
                                                    </div>
                                                    <div class="additional-remain" id="additional-cost" style="display: block;">
                                                        <ul>
                                                            <li>Additional data - $0.30/MB</li>
                                                            <li>Voicemail cost - $0.20</li>
                                                            <li>Additional Minutes - $0.69/Min</li>
                                                            <li>Calling International Top 5 - $0.91/Min</li>
                                                            <li>Calling Rest of World - $1.43/Min</li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                            <br>
                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">Extras</h3>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div class="bt_rs">
                                                    <a href="/onliness/extras--catalogue-/" class="add-button">ADD</a>
                                                </div>
                                                </div>
                                            </div>
                                       
                                            
                                        <div class="remain padding-top10">
                                            <h3>$15 Data</h3>
                                                <h4 class="renew-date padding-top8">Renews on 17 Jul 2015 - 9 days left</h4>
                                                <div class="row">
                                                    <div class="col-xs-3 no-padding">
                                                        <span class="remaining-data">
                                                            Data
                                                        </span>

                                                    </div>    
                                                    <div class="col-xs-4">
                                                        <span class="expires-data">
                                                            700MB
                                                        </span>
                                                    </div>
                                                </div>
                                                <button class="planextras-bttn nongr" type="submit" id="submit-1666123634" title="DON'T RENEW">DON'T RENEW</button>

                                                <hr>
                                                <h3>$6 My Favourites</h3>
                                                <h4 class="renew-date padding-top8">Renews on 17 Jul 2015 - 9 days left</h4>
                                                <div class="row no-paddin">
                                                    <div class="col-xs-3 no-padding">
                                                        <span class="remaining-data">
                                                            Number<br>
                                                            Free mins
                                                        </span>

                                                    </div>    
                                                    <div class="col-xs-5 my-favourites">
                                                        <span class="expires-data">
                                                            027 123 4569<br>
                                                            Up to 2 hours
                                                        </span>
                                                    </div>
                                                </div>
                                                <button class="planextras-bttn nongr" type="submit" id="submit-1666123634" title="DON'T RENEW">DON'T RENEW</button>

                                                <hr>
                                            </div>  

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>