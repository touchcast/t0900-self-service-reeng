<?php include("../includes/header-nav-buy-extras.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <?php include("../includes/top-header.php"); ?>
        
                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                </div>
                                <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <!-- Usage Panel -->

                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-12">
                                                    <h3 class="bar-module-headline-data">Data</h3>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$3</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$3 Data</h3>
                                                                    <ul class="clearfix">
                                                                        <li>30MB</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                        
                                                    </div>
                                                   
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$6</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$6 Data</h3>
                                                                    <ul class="clearfix">
                                                                        <li>60MB</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                        
                                                    </div>
                                                    
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                        <span class="remain-buy-extras-link" href="?selectedProduct=valuepack100122">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$9</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <div class="inner-value" style="float: left;">
                                                                        <h3>$9 Data</h3>
                                                                        <ul class="clearfix">
                                                                            <li>90MB</li>
                                                                            <!-- <li class="separator">+</li>
                                                                                <li>30 Minutes</li> -->
                                                                            </ul>
                                                                    </div>        
                                                                    <div class="data-price">    
                                                                        
                                                                            <div class="sash">
                                                                                <img src="../images/sash-device-a-bonus.png" alt="">
                                                                            </div>
                                                                            <!-- <div class="clearfix inner-card"> -->
                                                                                
                                                                                
                                                                                <!-- <div class="device-info"> -->

                                                                                    <div class="device-price-buy">
                                                                                        <span class="number">100</span><span class="txt">MB DATA</span>
                                                                                    </div>
                                                                                <!-- </div> -->
                                                                                <!-- <button type="button" class="slim blue">Buy now</button> -->
                                                                        <!--     </div> -->

                                                                            
                                                                        
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                            </div><!--row-->
                                            <div class="clear"></div>
                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-12">
                                                    <h3 class="bar-module-headline-data">Voice</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$3</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$3 Talk</h3>
                                                                    <ul class="clearfix">
                                                                        <li>15 Minutes</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$6</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <h3>$6 Talk</h3>
                                                                    <ul class="clearfix">
                                                                        <li>30 Minutes</li>
                                                                        <!-- <li class="separator">+</li>
                                                                            <li>30 Minutes</li> -->
                                                                        </ul>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    
                                                    <div class="remain-bay-valuepack" id="product_valuepack">
                                                        <span class="remain-buy-extras-link" href="?selectedProduct=valuepack100122">
                                                            <div class="remain-bex">
                                                                <div class="month-value-bex">
                                                                    <h2>$6</h2>
                                                                    <h4>per month</h4>
                                                                </div>
                                                                <div class="pack-value-bex">
                                                                    <div class="inner-value" style="float: left;">
                                                                        <h3>My Favourites</h3>
                                                                        <ul class="clearfix">
                                                                            <li>2 hours per call</li>
                                                                            <!-- <li class="separator">+</li>
                                                                                <li>30 Minutes</li> -->
                                                                            </ul>
                                                                    </div>        
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                            </div><!--row-->
                                            
                                             

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>
                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>