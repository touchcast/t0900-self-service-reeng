<?php include("../includes/header-nav-buy-extras.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <!-- <span class="cart-summary-text">Monthly plan payments</span> -->
                    <div class="leftnavigation-subpages navigation"><!-- Navigation -->

                    <?php include("../includes/sidenav-myplans-extras.php"); ?>

                    </div><!--leftnavigation navigation-->
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="right-content">
                        <div id="maincontent">
                                <?php include("../includes/top-header.php"); ?>
        
                                <div class="error-msg" id="internalError" style="display:none">
                                        <p id="internalErrorText"></p>
                                </div>
                                <div class="par parsys"><div class="myusage section">
                                                 
                                <div class="data-module-comp">
                                            <div class="row panel bar-module data-module">
                                                <div class="col-xs-8">
                                                    <h3 class="bar-module-headline-data">My Favourites</h3>
                                                </div>
                                                <div class="col-xs-4 text-right my-fav-number">
                                                    $6/mth per number
                                                </div>
                                            </div>
                                           <div class="row">
                                                <div class="col-xs-12 padding-top10 margin-bottom20">
                                                    <br/>
                                                    <div class="my-favourites-headline-calls">
                                                        Call any Spark mobile or landline number for up to 2 hours per call
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row plans">
                                                    <table>
                                                        <tr>
                                                            <td class="col-xs-3 left-column">Cost</td>
                                                            <td class="col-xs-9">$6/mth per number</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3 left-column">Renewal</td>
                                                            <td class="col-xs-9">Monthly</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-3 left-column">Calls</td>
                                                            <td class="col-xs-9">Up to 2 hours</td>
                                                        </tr>
                                                    </table>
                                                   
                                                </div>

                                                <!-- Popup Modal -->
                                                <div class="row">
                                                    <div class="col-md-12 text-center padding-top20">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="add-button" data-toggle="modal" data-target="#myModal">
                                                          Launch Modal
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>



                                            <div class="row">     
                                                <div class="col-xs-12 text-center padding-top15 margin-bottom20">
                                                    <div class="myplans-info-box">
                                                        Add up to 3 My Favourites numbers.<br/> You can see My Favourites numbers under <span class="no-wrap">"My Plan & Extras" section.</span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="no-padding">
                                                            <h3 class="purple-title">Enter a Spark mobile or landline</h3>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="row padding-top20">
                                                <div class="col-xs-12">

                                                    <div class="row">
                                                    <div class="phone-number">
                                                        <div class="prefix col-xs-2">
                                                            <div class="dropdown">
                                                                <div class="dropdown-number">
                                                                <button class="dropdown-toggle" type="button" id="prefix-number-1" data-toggle="dropdown">
                                                                027
                                                               
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="prefix-number-1">
                                                                    <li><a role="menuitem">021</a></li>
                                                                    <li><a role="menuitem">022</a></li>
                                                                    <li><a role="menuitem">027</a></li>
                                                                    <li><a role="menuitem">028</a></li>
                                                                    <li><a role="menuitem">029</a></li>
                                                                    <li><a role="menuitem">020</a></li>
                                                                </ul> 
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="mobile-number col-xs-10 margin-bottom15">
                                                            <input class="input-large" id="contact_number" type="text" placeholder="123 4568">
                                                            
                                                        </div>
                                                    </div>
                                                    </div><!--row-->


                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <input name="productId" value="" type="hidden">
                                                    <button class="bt-small-orange buy-extra-btn" type="submit" title="PAY MY BILL">BUY EXTRA</button>
                                                </div>
                                            </div>   
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="panel additional-cost margin-top15">
                                                        <a class="additional-link" data-id="additional-cost-hook" href="#" style="font-weight: normal;">
                                                        My Favourites terms & conditions</a>
                                                    </div>
                                                    <div class="additional-remain" id="additional-cost" style="display: block;">
                                                        <p class="terms-conditions">
                                                            Id quiant etur? Omnis ea cum quam, estorep eribus, omnistionet ra quiditem harion consequo ex experio. 
                                                            Ut venis dolupta tatusa volore volora aut as cones ditia as aut prempor atiatem sapernatiis mintia vollaccab 
                                                            imus, utetus ut aut eat aut fugit repre, sin re que ius expercimus dit moluptati adis eosam hil explit qui cus, 
                                                            quid es doluptibusda quamus perro et latus 
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>    
                                            <br/>
                                            <div class="clear"></div>
                                            

                                            
                                             

                                <!-- End Usage Panel -->                    
                                        </div>
                                  
                                  
                                    </div>

                        </div>

                        <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- <a type="button" class="close btn-mobile"><i class="icon-cancel-circle blue-text"></i></a> -->

    <div class="modal-dialog adding-number-modal added-number-modal">
        <div class="modal-content">
             <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



          <!-- <div class="modal-header">
            
          </div> -->

          <div class="modal-body text-center">
             <!-- <a href="#" class="float-right clearfix" data-dismiss="modal">
                Close <i class="icon-x"></i>
            </a> -->

            <span class="added-number-success"><b>All done!</b></span>

            <div class="added-number-success-inner">027 123 4568 has been added to My Favourites. <br/> It's good to go.</div>

            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <a href="/onliness/extras--catalogue-/" data-dismiss="modal" class="add-button">OK</a>  
                </div>
            </div>
              
            <!-- <a href="#" class="button blue"> call to action</a> -->
            <!-- <div class="modal-header">
                
            </div> -->
          </div>
          
        </div>
    </div>
</div>
<!-- /End Popup Modal -->





                        </div><!--maincontent-->
                    </div>
                </div>
            </div>    

        </div>


<?php include("../includes/footer.php"); ?>