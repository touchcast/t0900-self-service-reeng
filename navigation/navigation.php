<?php include("../includes/header-nav-navigation.php"); ?>


        <!-- <div class="page-header">
            <div class="center-wrap">
                <h1>My Mobile</h1>
            </div>
        </div> -->


        <div class="container-fluid global-style checkout-step-4">

            <div class="inner-card">

                <div class="leftnavigation navigation"><!-- Navigation -->
                    <div id="leftmarginal">
                    </div>  
                        <div class="clear"></div>
                        
                        <div id='cssmenu' class="hidden-sm hidden-xs">
                            <h3 class="side-nav-title">My Mobile</h3>
                            <ul>
                            <!--    <li><a href='#'><span>My Spark</span></a></li> -->
                               <li class='active has-sub'><a href='#'><span>My Usage</span></a>

                                  <ul>
                                     <li><a href='#'><span>View Data Activity</span></a>
                                        <!-- <ul>
                                           <li><a href='#'><span>Sub Product</span></a></li>
                                           <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                        </ul> -->
                                     </li>
                                     <!-- <li class='has-sub'><a href='#'><span>Product 2</span></a>
                                        <ul>
                                           <li><a href='#'><span>Sub Product</span></a></li>
                                           <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                        </ul>
                                     </li> -->
                                  </ul>
                               </li>
                               <li class="has-sub"><a href='#'><span>My Plan & Extras</span></a>
                                    <ul>
                                        <li><a href='#'><span>Extras Purchased</span></a></li>
                                    </ul>
                               </li>
                               <li><a href="#" title="Buy Extras">Buy Extras</a></li>
                               <li class="has-sub"><a href='#'><span>Payments</span></a>
                                    <ul>
                                        <li><a href='#'><span>Top up a Prepaid</span></a></li>
                                    </ul>
                                </li>
                               <li class="has-sub"><a href='#'><span>My Settings</span></a>
                                    <ul>
                                        <li><a href='#'><span>Change Password</span></a></li>
                                    </ul>
                               </li>
                            </ul>
                    </div><!--cssmenu-->
                <!-- End Self-Service Navigation --> 

                </div><!--leftnavigation navigation-->

            </div><!--innercard-->    

            <!--Summary DIV-->
            <!-- <div class="inner-container">
                <div class="card clearfix secondary">
                    <div class="inner-card clearfix tight">
                        <h2>Summary</h2>

                        <div class="cart-summary-row">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span class="cart-summary-text">Monthly plan payments</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="cart-summary-text text-right">
                                        $99.00
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cart-summary-row">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span class="cart-summary-text">Total up-front payment</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="cart-summary-text text-right">
                                        $1000.00
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cart-summary-row">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span class="cart-summary-text">Total</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="cart-summary-text text-right">
                                        $1299.00
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="info-box green" style="margin-bottom:20px;">
                        <h5>Heads-up.</h5> The up-front payments will be added to your next Spark bill with account no. <account number>. Look out for your bill arriving to find out how long you have to pay.
                        </div>

                        <div class="clearfix">
                            <input type="checkbox" name="terms" class="normal" id="terms">
                            <label for="terms" class="no-margin"><span></span>I agree to the <a href="#">Terms and Conditions</a>.</label>
                        </div>

                        <div class="clearfix">
                            <input type="checkbox" name="opt_in" class="normal" id="opt_in">
                            <label for="opt_in" class="no-margin"><span></span>Please email me updates about any new services, news, offers and exclusive promotions.</label>
                        </div>
                    </div>
                </div>
            </div> --><!--inner-container-->
        </div>

<?php include("../includes/footer.php"); ?>