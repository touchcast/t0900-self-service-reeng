<?php include("../includes/header-nav-my-settings.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php include("../includes/sidenav-my-settings.php"); ?>
            </div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Data usage alerts</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff no" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>If you reach 80% or 100% of your data limit, we'll let you know.</p>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Alerts for Extras</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff no" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>Get alerts when an Extra needs renewing, when it's about to expire and when you restart or cancel it.</p>
                                    </div>
                                </div>
                                
                           
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">NZ Data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-xs-12 margin-bottom15">$150 Current limit</div> -->
                                    <div class="col-xs-6 margin-top20 margin-bottom15"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 margin-top20 margin-bottom15 mobile-text"><p>Your current limit</p></div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Overseas data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 margin-top20 margin-bottom15"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 margin-top20 margin-bottom15 mobile-text"><p>Your current limit</p></div>
                                </div>

                                
                               
                    		</div>
                    	</div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer.php"); ?>