<?php include("../includes/header-nav-my-settings.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
            	<?php include("../includes/sidenav-my-settings.php"); ?>
            </div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="right-content">
				<div id="maincontent">
					<!--Top Header-->
                    <?php include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                    	<div class="myusage section">
                    		<div class="data-module-comp">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Data usage alerts</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff no" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>If you reach 80% or 100% of your data limit, we'll let you know.</p>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Alerts for Extras</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff no" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>Get alerts when an Extra needs renewing, when it's about to expire and when you restart or cancel it.</p>
                                    </div>
                                </div>
                               <!-- Popup Modal -->
                                                <div class="row">
                                                    <div class="col-md-12 text-center padding-top20">
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="add-button" data-toggle="modal" data-target="#myModal">
                                                          Launch Modal
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>
                                
                           
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">NZ Data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-top15">
                                    <!-- <div class="col-xs-12 margin-bottom15">$150 Current limit</div> -->
                                    <div class="col-xs-6"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 mobile-text"><p>Your current limit</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                    <P>Choose your new limit:</P>
                                    <div class="row margin-bottom20 select-amount padding-top5">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">
                                            <a href="/onliness/extras--catalogue-/" class="add-button add-button-grey">$0</a>  
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">    
                                            <a href="/onliness/extras--catalogue-/" class="add-button active">$10</a>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount">
                                            <a href="/onliness/extras--catalogue-/" class="add-button">$20</a>  
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 amount row-last-btn fifty-dollar">    
                                            <a href="/onliness/extras--catalogue-/" class="top-up-amount active">$50</a>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 last-btn amount">    
                                            <a href="/onliness/extras--catalogue-/" class="add-button unlimited-button">$100</a>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row padding-top15">
                                    <div class="col-xs-12  margin-bottom15">
                                        <input name="productId" value="" type="hidden">
                                        <button class="bt-small-orange buy-extra-btn" type="submit" title="PAY MY BILL">Update data limit</button>
                                    </div>
                                </div>  
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Overseas data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 margin-top20 margin-bottom15"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 margin-top20 margin-bottom15 mobile-text"><p>Your current limit</p></div>
                                </div>
                               
                    		</div>
                    	</div>
                    </div>
				</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- <a type="button" class="close btn-mobile"><i class="icon-cancel-circle blue-text"></i></a> -->

    <div class="modal-dialog adding-number-modal added-number-modal">
        <div class="modal-content">
             <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



          <!-- <div class="modal-header">
            
          </div> -->

          <div class="modal-body text-center">
             <!-- <a href="#" class="float-right clearfix" data-dismiss="modal">
                Close <i class="icon-x"></i>
            </a> -->

            <span class="added-number-success"><b>Success!</b></span>

            <div class="added-number-success-inner">Your new data limit has been applied!</div>

            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <a href="/onliness/extras--catalogue-/" data-dismiss="modal" class="add-button">OK</a>  
                </div>
            </div>
              
            <!-- <a href="#" class="button blue"> call to action</a> -->
            <!-- <div class="modal-header">
                
            </div> -->
          </div>
          
        </div>
    </div>
</div>
<!-- /End Popup Modal -->






			</div>
		</div>
	</div>
</div>
<?php include("../includes/footer.php"); ?>