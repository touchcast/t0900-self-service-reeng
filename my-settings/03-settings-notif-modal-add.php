<?php include("../includes/header-nav-my-settings.php"); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
            <div class="leftnavigation-subpages navigation">
                <?php include("../includes/sidenav-my-settings.php"); ?>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="right-content">
                <div id="maincontent">
                    <!--Top Header-->
                    <?php include("../includes/top-header.php"); ?>
                    <div class="par parsys">
                        <div class="myusage section">
                            <div class="data-module-comp">
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Data usage alerts</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff yes" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>If you reach 80% or 100% of your data limit, we'll let you know.</p>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Alerts for Extras</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <form action="" method="post">
                                                <button type="submit" value="true" name="isOn" class="onoff yes" title="On/Off Switch">
                                                    <span class="bt-label">YES</span><span class="yn-switcher"></span>
                                                </button>
                                                <input name="action" value="notifications%2Fupdate%3Ftype%3Dusage_alert" type="hidden">
                                                <input name="is_on" value="false" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom15">
                                        <p>Get alerts when an Extra needs renewing, when it's about to expire and when you restart or cancel it.</p>
                                    </div>
                                </div>

                                <!-- Popup Modal -->
                                <div class="row">
                                    <div class="col-md-12 text-center padding-top20">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="add-button" data-toggle="modal" data-target="#myModal">
                                          Launch Modal
                                        </button>
                                    </div>
                                </div> 
                                <br/>  
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-12">
                                        <h3 class="bar-module-headline-data">Send alerts to:</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom10">
                                        <h4 class="renew-date">You can add up to 3 mobile numbers</h4>
                                    </div>
                                </div>
                                <div class="row row-separate">     
                                    <div class="col-xs-6">
                                        <strong class="my-settings-mobile-number">027 123 4567</strong>
                                    </div>
                                    <div class="col-xs-6 mobile-text-number">
                                        <p>Your mobile number</p>
                                    </div>
                                </div>
                                <!-- <div class="row row-separate">     
                                    <div class="col-xs-6">
                                        027 123 4568
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="#">Remove</a>
                                    </div>
                                </div> -->
                                <div class="row">     
                                    <div class="col-xs-6 margin-top10">
                                        <input id="" type="text" placeholder="027 123 4568">
                                    </div>
                                    <div class="col-xs-3 col-sm-6 col-xs-offset-3 col-sm-offset-0 margin-top10">
                                        <a id="show-more-button" class="add-button custom-add-btn">Add</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 margin-top20 margin-bottom10">
                                        <h4 class="renew-date">You can add up to 4 email addresses</h4>
                                    </div>
                                </div>
                                <div class="row">     
                                    <div class="col-xs-6 margin-bottom40">
                                        <input id="" type="text" placeholder="Enter email addresses">
                                    </div>
                                    <div class="col-xs-3 col-sm-6 col-xs-offset-3 col-sm-offset-0 margin-bottom40">
                                        <a id="show-more-button" class="add-button add-button-grey custom-add-btn">Add</a>
                                    </div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">NZ Data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 margin-top20 margin-bottom15"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 margin-top20 margin-bottom15 mobile-text"><p>Your current limit</p></div>
                                </div>
                                <div class="row panel bar-module data-module">
                                    <div class="col-xs-8">
                                        <h3 class="bar-module-headline-data">Overseas data limit</h3>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="bt_rs">
                                            <a href="#" class="add-button">Change</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 margin-top20 margin-bottom15"><strong class="my-settings-mobile">$20</strong></div>
                                    <div class="col-xs-6 margin-top20 margin-bottom15 mobile-text"><p>Your current limit</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--maincontent-->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- <a type="button" class="close btn-mobile"><i class="icon-cancel-circle blue-text"></i></a> -->

    <div class="modal-dialog adding-number-modal">
        <div class="modal-content">
             <!-- <a type="button" class="close btn-desk" data-dismiss="modal"><i class="icon-cancel-circle">Close</i></a> -->



          <!-- <div class="modal-header">
            
          </div> -->

          <div class="modal-body text-center">
             <!-- <a href="#" class="float-right clearfix" data-dismiss="modal">
                Close <i class="icon-x"></i>
            </a> -->

            <span class="adding-number-title"><b>Adding a mobile number</b></span>

            <div class="adding-number">Happy to send alerts<br/>to 027 123 45687</div>

            <div class="row">
                <div class="col-xs-4 col-xs-offset-2">
                    <a href="/onliness/extras--catalogue-/" data-dismiss="modal" class="add-button">NO</a>  
                </div>
                <div class="col-xs-4 col-xs-preset-2">    
                    <a href="/onliness/extras--catalogue-/" class="add-button">YES</a>
                </div>
            </div>
              
            <!-- <a href="#" class="button blue"> call to action</a> -->
            <!-- <div class="modal-header">
                
            </div> -->
          </div>
          
        </div>
    </div>
</div>
<!-- /End Popup Modal -->



            </div>
        </div>
    </div>
</div>
<?php include("../includes/footer.php"); ?>